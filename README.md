# NFL Files

This is a list of the type of files with examples of each one that we use to bootstrap our applications with data about a game or a schedule of games.

## Boxscore Combined

Received 30-45 minutes after games end. These contain all the finalized stats of a game. I think you guys already did a mapping on this one in our first trial run.

File: *ABX-BOX-CIN-BAL-COMBINED.xml*

## Recaps

Received after the games end. We get one for every game.

File: *ABD-RECAP-PIT-BAL.xml*.

## Team Stats

Received after the conclusion of the week's games for every team.

File: *ABX-TEAMSTATS-CLE.xml* - This is one example for the Browns, but we get them for every team. We get a new one when the stats change.

## "JFILE" schedules

These are the weekly schedules that we receive and use as the basis of our "latest schedule" endpoint. Intended to be a one-stop shop for info about the most recent week of games. We receive these on a daily basis and sometimes multiple times daily even if nothing has changed.

File: *BC-ABJ.xml*

## Team Schedules

Received for every team in the league. We get updates on a daily basis.

File: *ABX-NOS-SKED.xml* - Example for New Orleans Saints.

## Yearly Schedules

Received daily and separated by league.

Files: *ABX-AFC-YEARLY.xml* and *ABX-NFC-YEARLY.xml*.

## Depth Charts, Rosters, Injuries by Division

Received on a weekly basis at various intervals.

- *ABX-DEPTH-ARI.xml* - This example is for the Cardinals, but we get a depth chart for every team. And we get a new one when the depth chart changes.
- *ABX-ROSTER-ARI.xml* - We get a roster for every team. A new roster file is sent when the roster changes.
- *ABX-AFC-HURTS.xml* and *ABX-NFC-HURTS.xml* - Contains injury listings for every team in each league.

## Odds

Received as vegas odds are changed and updated.

File: *ABO-NFL-ODDS2.xml*

## Previews

Received a few days before the start of each game. Received for each game.

File: *ABV-PREVIEW-MIA-BUF.xml*

## Leading Passers, Rushers, and Receivers by league

Stats received for these three categories and separated by league.

- Passing Leaders - *ABX-NFC-PASSERS.xml* and *ABX-AFC-PASSERS.xml*
- Rushing Leaders - *ABX-NFC-RUSHERS.xml* and *ABX-AFC-RUSHERS.xml*
- Receiving Leaders - *ABX-NFC-RECYARDS.xml* and *ABX-AFC-RECYARDS.xml*

## Live Game Files

These files are received in as near real time as possible as games are live. The problem with these files is that the schema can change depending on what type of play occurs. This is going to be the most challenging piece of this integration. I will try to get a definitive schema that covers all cases, but this is not something we've been able to get from our provider in the past.

- Game Underway - *AB-4583_0.XML* - The number in the file name (4583) is the GameID of the game in question. The number after the underscore is the sequence ID of the play. It's okay if these use your system's IDs, but we'll have to make sure that is translated in in all the other files where the ID is used.
- Typical Play - *AB-4583_118.XML* - "2-yard pass from Joe Flacco to Owen Daniels." is what happened on this play. There are nodes describing the passer, the receiver, and the tackler.
- Timeout Play - *AB-4583_119-NP.XML* - The NP indicates "No Play."
